Locally owned and operated, Large Lift Trucks provides used forklift sales and rentals at competitive prices. We’re your one-stop-shop for your forklift needs. Whether you want to buy a new or used forklift, rent or lease one, need repairs or services, or need forklift parts, we’ve got you covered.

Address: 7942 Rand St, Houston, TX 77028, USA

Phone: 866-918-3372

Website: https://largelifttrucks.com
